const additive = require('./additive');

test('101 is additive', () => {
  expect(additive('112358')).toBeTruthy();
});

// test('101 is additive', () => {
//   expect(additive('101')).toBeTruthy();
// });
//
// test('111 is not additive', () => {
//   expect(additive('111')).toBeFalsy();
// });
//
// test('1111 is not additive', () => {
//   expect(additive('1111')).toBeFalsy();
// });
//
// test('1236 is not additive', () => {
//   expect(additive('1111')).toBeTruthy();
// });
//
